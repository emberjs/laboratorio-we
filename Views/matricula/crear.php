
<?php
include_once ('Views/header.php');
?>


<div class="container">

    <div class="row">
        <div class="col-xs-12 col-sm-8 col-md-6 col-sm-offset-2 col-md-offset-3">
            <form role="form" id="estudentForm" method="post" action="/<?php echo __ROOT_BASE__ ?>/matricula/guardar">
                <h2>Matricular Estudiante</h2>
                <hr class="colorgraph">
                <span style="color: red;"  id="errorMatricula"></span>
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="form-group">
                            <label for="alumno">Estudiante</label>
                            <select class="form-control input-lg" id="alumno" name="alumno">
                                <option value="0">Seleccione uno</option>
                                <?php
                                    foreach ($estudiantes as $estudiante)
                                    {
                                        ?>
                                        <option value="<?php  echo $estudiante['id'];?>"><?php echo $estudiante['identificasion'] ?> / <?php echo $estudiante['nombres'] ?> <?php echo $estudiante['apellidos'] ?></option>
                                        <?php
                                    }
                                ?>
                            </select>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="form-group">
                            <label for="materia">Materias</label>
                            <select class="form-control input-lg" id="materia" name="materia">
                                <option value="0">Seleccione uno</option>
                                <?php
                                foreach ($materias as $materia)
                                {
                                    ?>
                                    <option value="<?php  echo $materia['id'];?>"><?php echo $materia['descripcion'] ?> </option>
                                    <?php
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-4 col-sm-3 col-md-3">
                        <button id="submit" type="submit" class="btn btn-success" data-color="info" tabindex="7">Guardar</button>
                    </div>
                    <div class="col-xs-4 col-sm-3 col-md-3">
                        <a href="/<?php echo __ROOT_BASE__ ; ?>/matricula" class="btn btn-info" data-color="info" tabindex="7">Atras</a>
                    </div>
                </div>
            </form>
        </div>
    </div>


    <?php
    include_once ('Views/footer.php');
    ?>

    <script>

        $('#submit').attr("disabled", true);

        $( "#materia" ).change(function(event) {
            var materia = event.currentTarget.value;
            var estudiante = $('#alumno').val();
            validate(estudiante, materia);
        });

        $( "#alumno" ).change(function(event) {
            var materia =  $('#materia').val();
            var estudiante =  event.currentTarget.value;
            validate(estudiante, materia);
        });

        function validate(estudiante, materia) {
            // documento = $('#numdocumento').val();
            var info = {
                'estudiante': estudiante,
                'materia': materia
            };
            url = <?php echo "'/" . __ROOT_BASE__ . "'"; ?> +"/ajax/matricula";
            if(estudiante != 0 && materia != 0)
            {
                $.ajax({
                    url: url,
                    method: 'post',
                    data: info
                }).done(function (data) {
                    data = JSON.parse(data);
                    if(data.exist == true)
                    {
                        $('#submit').attr("disabled", true);
                        $("#errorMatricula").html('El Alumno ya se matricula a dicha materia')
                    }else{
                        $('#submit').attr("disabled", false);
                        $("#errorMatricula").html('')
                    }
                });
            }
        }


    </script>
