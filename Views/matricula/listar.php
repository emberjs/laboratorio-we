<?php
include_once('Views/header.php');
?>
<div class="page-header">
    <h1>Gestionar Matricula</h1>
</div>
<div class="col col-md-12">
    <div>
        Agregar <a href="/<?php echo __ROOT_BASE__ ?>/matricula/crear" class="btn btn-success"> <i class="glyphicon glyphicon-plus-sign"></i></a>
    </div>
    <br>
    <table id="matriculaTable" class="table table-striped table-bordered" style="width:100%">
        <thead>
        <tr>
            <th>DNI</th>
            <th>Nombres</th>
            <th>Apellidos</th>
            <th>Materia</th>
            <th>Acciones</th>
        </tr>
        </thead>
        <tbody>
        <?php
        foreach ($data as $item) {
            ?>
            <tr>
                <td><?php echo $item['dni']; ?></td>
                <td><?php echo $item['nombres']; ?></td>
                <td><?php echo $item['apellidos']; ?></td>
                <td><?php echo $item['materia']; ?></td>
                <td>
                    <a class="btn btn-danger" href="/<?php echo __ROOT_BASE__ ?>/matricula/<?php echo$item['id']; ?>/eliminar"><i class="glyphicon glyphicon-remove-sign"></i></a>
                </td>
            </tr>
            <?php
        }
        ?>
        </tbody>
    </table>
</div>

<?php
include_once ('Views/footer.php');
?>

<script>
    $('#matriculaTable').DataTable();
</script>
