<?php
include_once ('Views/header.php');
?>


<div class="container">
    <div class="row">
        <div class="col-xs-12 col-sm-8 col-md-6 col-sm-offset-2 col-md-offset-3">
            <form role="form" id="usersForm" method="post" action="/<?php echo __ROOT_BASE__ ?>/users/guardar">
                <h2>Registrar Usuario</h2>
                <hr class="colorgraph">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="form-group">
                            <label for="usuario">Usuario / Correo Electronico (Email)</label>
                            <input type="text" name="usuario" id="usuario" class="form-control input-lg" placeholder="Usuario / Correo Electronico (Email)" tabindex="1" required>
                            <span style="color: red;"  id="erroruser"></span>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 col-sm-6 col-md-6">
                        <div class="form-group">
                            <label for="contrasena">Contraseña</label>
                            <input type="password" name="contrasena" id="contrasena" class="form-control input-lg" placeholder="Contraseña" tabindex="5" required>
                            <span style="color: red;"  id="errorContrasena"></span>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-6">
                        <div class="form-group">
                            <label for="contrasena2">Repetir Contraseña</label>
                            <input type="password" name="contrasena2" id="contrasena2" class="form-control input-lg" placeholder="Repetir Contraseña" tabindex="6" required>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-4 col-sm-3 col-md-3">
                        <button id="submit" type="submit" class="btn btn-success" data-color="info" tabindex="7">Guardar</button>
                    </div>
                    <div class="col-xs-4 col-sm-3 col-md-3">
                        <a href="/<?php echo __ROOT_BASE__ ; ?>/users" class="btn btn-info" data-color="info" tabindex="7">Atras</a>
                    </div>
                </div>
            </form>
        </div>
    </div>


    <?php
    include_once ('Views/footer.php');
    ?>

    <script>
        $('#submit').attr("disabled", true);
        $('#contrasena').attr("disabled", true);
        $('#contrasena2').attr("disabled", true);

        $('#contrasena').keyup(function(event){
            var contrasena = $('#contrasena').val();
            var repead = $('#contrasena2').val();
            isEqualPassword(contrasena, repead);
        });
        $('#contrasena2').keyup(function(event){
            var contrasena = $('#contrasena').val();
            var repead = $('#contrasena2').val();
            isEqualPassword(contrasena, repead);
        });

        function isEqualPassword(contrasena, repead) {
            if(contrasena !== repead){
                $("#errorContrasena").html('Las contraseñas no son iguales')
                $('#submit').attr("disabled", true);
            }else{
                $("#errorContrasena").html('')
                $('#submit').attr("disabled", false);
            }
        }

        $('#usuario').keyup(function(event){
            usuario = $('#usuario').val();
            url = <?php echo "'/".__ROOT_BASE__."'"; ?> + "/ajax/users";
            if(usuario.length >= 5)
            {
                $.ajax({
                    url: url,
                    method: 'post',
                    data: {'user' : usuario}
                }).done(function(data) {
                    data = JSON.parse(data);
                    if(data.exist == true)
                    {
                        $('#contrasena').attr("disabled", true);
                        $('#contrasena2').attr("disabled", true);
                        $('#submit').attr("disabled", true);
                        $("#erroruser").html('El DNI ya esta en uso')
                    }else{

                        $('#contrasena').attr("disabled", false);
                        $('#contrasena2').attr("disabled", false);
                        $("#erroruser").html('')
                    }
                });
            }else{
                $('#contrasena').attr("disabled", true);
                $('#contrasena2').attr("disabled", true);
                $('#submit').attr("disabled", true);
                $("#erroruser").html('El User / Email debe tener minimo 5 caracteres')
            }

        });
    </script>

