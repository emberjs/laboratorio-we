<?php
include_once ('Views/header.php');
?>


<div class="container">
    <div class="row">
        <div class="col-xs-12 col-sm-8 col-md-6 col-sm-offset-2 col-md-offset-3">
            <form role="form" id="usersForm" method="post" action="/<?php echo __ROOT_BASE__ ?>/users/<?php echo $identify ; ?>/guardar">
                <?php
                    if($data)
                    {
                        ?>
                        <h2>Editar Usuario ---  <?php echo $data[0]['usuario']?></h2>
                        <hr class="colorgraph">
                        <div class="row">
                            <div class="col-xs-12 col-sm-6 col-md-6">
                                <div class="form-group">
                                    <label for="contrasena">Contraseña</label>
                                    <input type="password" name="contrasena" id="contrasena" class="form-control input-lg" placeholder="Contraseña" tabindex="5" required>
                                    <span style="color: red;"  id="errorContrasena"></span>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-6 col-md-6">
                                <div class="form-group">
                                    <label for="contrasena2">Repetir Contraseña</label>
                                    <input type="password" name="contrasena2" id="contrasena2" class="form-control input-lg" placeholder="Repetir Contraseña" tabindex="6" required>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-4 col-sm-3 col-md-3">
                                <button id="submit" type="submit" class="btn btn-success" data-color="info" tabindex="7">Guardar</button>
                            </div>
                            <div class="col-xs-4 col-sm-3 col-md-3">
                                <a href="/<?php echo __ROOT_BASE__ ; ?>/users" class="btn btn-info" data-color="info" tabindex="7">Atras</a>
                            </div>
                        </div>
                        <?php
                    }
                    else
                    {
                        ?>
                        <div class="alert alert-info" role="alert">
                            El Usuario buscado no existe
                            <a href="/<?php echo __ROOT_BASE__ ; ?>/users" class="btn btn-info" data-color="info" tabindex="7"> Cancelar</a>
                        </div>
                        <?php
                    }
                    ?>


            </form>
        </div>
    </div>


    <?php
    include_once ('Views/footer.php');
    ?>

    <script>
        $('#submit').attr("disabled", true);

        $('#contrasena').keyup(function(event){
            var contrasena = $('#contrasena').val();
            var repead = $('#contrasena2').val();
            isEqualPassword(contrasena, repead);
        });
        $('#contrasena2').keyup(function(event){
            var contrasena = $('#contrasena').val();
            var repead = $('#contrasena2').val();
            isEqualPassword(contrasena, repead);
        });

        function isEqualPassword(contrasena, repead) {
            if(contrasena !== repead){
                $("#errorContrasena").html('Las contraseñas no son iguales')
                $('#submit').attr("disabled", true);
            }else{
                $("#errorContrasena").html('')
                $('#submit').attr("disabled", false);
            }
        }
    </script>

