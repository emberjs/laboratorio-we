<?php
    include_once('Views/header.php');
?>

    <div class="page-header">
        <h1>Usuarios</h1>
    </div>

    <div class="col col-md-12">
        <div>
            Agregar <a href="/<?php echo __ROOT_BASE__ ?>/users/crear" class="btn btn-success"> <i class="glyphicon glyphicon-plus-sign"></i></a>
        </div>
        <br>
        <table id="usersTable" class="table table-striped table-bordered" style="width:100%">
            <thead>
            <tr>
                <th>usuario / Correo electronico</th>
                <th>Acciones</th>
            </tr>
            </thead>
            <tbody>
            <?php
            foreach ($data as $item) {
                ?>
                <tr>
                    <td><?php echo $item['usuario']; ?></td>
                    <td>
                        <a class="btn btn-primary" href="/<?php echo __ROOT_BASE__ ?>/users/<?php echo$item['id']; ?>/editar"><i class="glyphicon glyphicon-pencil"></i></a>
                    </td>
                </tr>
                <?php
            }
            ?>
            </tbody>
        </table>
    </div>


<?php
include_once ('Views/footer.php');
?>

<script>
    $('#usersTable').DataTable();
</script>
