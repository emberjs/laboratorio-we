<?php
include_once ('Views/header.php');
?>
<div class="page-header">
    <h1>Gestionar Materias</h1>
</div>
    <div class="col col-md-12">
        <div>
            Agregar <a href="/<?php echo __ROOT_BASE__ ?>/materias/crear" class="btn btn-success"> <i class="glyphicon glyphicon-plus-sign"></i></a>
        </div>
        <br>
        <table id="idmaterias" class="table table-striped table-bordered" style="width:100%">
            <thead>
                <tr>
                    <th>Descripcion</th>
                    <th>Horas</th>
                    <th>Estado</th>
                    <th>Acciones</th>
                </tr>
            </thead>
            <tbody>
                <?php
                    foreach ($data as $item) {
                        $estado = 'inactive';
                        if($item['estado'] == 1 )
                        {
                            $estado = 'active';
                        }
                        ?>
                            <tr>
                                <td><?php echo $item['descripcion']; ?></td>
                                <td><?php echo $item['horas']; ?></td>
                                <td><?php echo $estado; ?></td>
                                <td>
                                    <a class="btn btn-primary" href="/<?php echo __ROOT_BASE__ ?>/materias/<?php echo$item['id']; ?>/editar"><i class="glyphicon glyphicon-pencil"></i></a>
                                    <a class="btn btn-danger" href="/<?php echo __ROOT_BASE__ ?>/materias/<?php echo$item['id']; ?>/eliminar"><i class="glyphicon glyphicon-remove-sign"></i></a>
                                    <a class="btn btn-warning" href="/<?php echo __ROOT_BASE__ ?>/materias/<?php echo$item['id']; ?>/ver"><i class="glyphicon glyphicon-search"></i></a>
                                </td>
                            </tr>
                        <?php
                    }
                ?>

            </tbody>
        </table>
    </div>



<?php
include_once ('Views/footer.php');
?>

<script>
    $('#example').DataTable();
</script>

