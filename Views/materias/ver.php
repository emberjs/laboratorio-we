<?php
include_once ('Views/header.php');
?>
    <div class="row" style="margin: 0 auto" >
        <div class="col-xs-12 col-sm-8 col-md-6 col-sm-offset-2 col-md-offset-3" >
            <?php
                if($data)
                {
                    ?>
                        <div class="panel panel-primary" style="margin: 0 auto">
                        <div class="panel-heading">
                            <h3 class="panel-title"><?php echo $data[0]['descripcion'] ?>  <?php echo $data[0]['horas'] ?></h3>
                        </div>
                        <div class="panel-body">
                            <div>
                                <b>Descripción de la Materia :</b>  <?php  echo $data[0]['descripcion'] ?>
                            </div>
                            <div>
                                <b>Horas por Materia :</b>  <?php echo $data[0]['horas'] ?> 
                            </div>
                           
                            
                        </div>
                    </div>
                    <?php
                }
                else
                {
                    ?>
                        <div class="alert alert-info" role="alert">
                            La Materia buscada no existe
                        </div>
                    <?php
                }
            ?>
        </div>
        <div class="clearfix"></div>
        <div class="col-xs-12 col-sm-8 col-md-6 col-sm-offset-2 col-md-offset-3">
            <a href="/<?php echo __ROOT_BASE__ ; ?>/materias" class="btn btn-info" data-color="info" tabindex="7">Atras</a>
        </div>
    </div>

<?php
include_once ('Views/footer.php');
?>

