
<?php
    include_once ('Views/header.php');
?>


<div class="container">

    <div class="row">
        <div class="col-xs-12 col-sm-8 col-md-6 col-sm-offset-2 col-md-offset-3">
            <form role="form" id="MateriaForm" method="post" action="/<?php echo __ROOT_BASE__ ?>/materias/guardar">
                <h2>Registrar Materia</h2>
                <hr class="colorgraph">
                <div class="row">
                    <div class="col-xs-12 col-sm-6 col-md-6">
                        <div class="form-group">
                            <label for="descripcion">Descripcion Materia</label>
                            <input type="text" name="descripcion" id="descripcion" class="form-control input-lg" placeholder="Descripcion de la Materia" tabindex="1" required>
                           
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-6">
                        <div class="form-group">
                            <label for="horas">Horas</label>
                            <input type="number" name="horas" id="horas" class="form-control input-lg" placeholder="Horas" tabindex="2" required>
                        </div>
                    </div>
                </div>
               
                
                <div class="row">
                    <div class="col-xs-4 col-sm-3 col-md-3">
                            <button id="submit" type="submit" class="btn btn-success" data-color="info" tabindex="3">Guardar</button>
                    </div>
                    <div class="col-xs-4 col-sm-3 col-md-3">
                        <a href="/<?php echo __ROOT_BASE__ ; ?>/materias" class="btn btn-info" data-color="info" tabindex="3">Atras</a>
                    </div>
                </div>
            </form>
        </div>
    </div>


<?php
    include_once ('Views/footer.php');
?>

<script>

        $('#submit').attr("disabled", false);
        $('#descripcion').keyup(function(event){
            descripcion = $('#descripcion').val();
            url = <?php echo "'/".__ROOT_BASE__."'"; ?> + "/ajax/materias";
           
                $.ajax({
                    url: url,
                    method: 'post',
                    data: {'id' : descripcion}
                }).done(function(data) {
                    data = JSON.parse(data);
                    if(data.exist == true)
                    {
                        console.log(data.exist);
                        $('#submit').attr("disabled", true);
                       // $("#errorDni").html('la materia esta en uso')
                    }else{
                        console.log(data.exist);
                        $('#submit').attr("disabled", false);
                       
                    }
                });
            

        });


    </script>

   