
<?php
include_once ('Views/header.php');
?>


<div class="container">

    <div class="row">
        <div class="col-xs-12 col-sm-8 col-md-6 col-sm-offset-2 col-md-offset-3">
            <form role="form" method="post" action="/<?php echo __ROOT_BASE__ ?>/materias/<?php echo $identify ; ?>/guardar">
                <h2>Editar Materias</h2>
                <hr class="colorgraph">
                <?php
                    if($data)
                    {
                        ?>
                        <input type="hidden" value="<?php echo $identify ; ?>">
                            <div class="row">
                                <div class="col-xs-12 col-sm-6 col-md-6">
                                    <div class="form-group">
                                        <label for="descripcion">Descripcion de la Materia</label>
                                        <input disabled type="text" name="descripcion" id="descripcion" class="form-control input-lg" placeholder="Descripcion de la Materia" tabindex="1" value="<?php echo $data[0]['descripcion']; ?>" required>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-6 col-md-6">
                                    <div class="form-group">
                                        <label for="horas">Horas</label>
                                        <input type="number" name="horas" id="horas" class="form-control input-lg" placeholder="Horas" tabindex="2" value="<?php echo $data[0]['horas']; ?>" required>
                                    </div>
                                </div>
                            </div>
                            
                           
                        <?php
                    }
                    else
                    {
                        ?>
                            <div class="alert alert-info" role="alert">
                                El Materia no Existe
                            </div>
                        <?php
                    }
                ?>

                <div class="row">
                    <?php
                        if($data)
                        {
                            ?>
                                <div class="col-xs-4 col-sm-3 col-md-3">
                                    <button type="submit" class="btn btn-success" data-color="info" tabindex="7">Guardar</button>
                                </div>
                            <?php
                        }
                    ?>

                    <div class="col-xs-4 col-sm-3 col-md-3">
                        <a href="/<?php echo __ROOT_BASE__ ; ?>/materias" class="btn btn-info" data-color="info" tabindex="7">Atras</a>
                    </div>
                </div>
            </form>
        </div>
    </div>

    <?php
    include_once ('Views/footer.php');
    ?>

