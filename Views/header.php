<html lang="es">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Calificasiones</title>
        <link href="/<?php echo __ROOT_BASE__ ?>/Assets/css/bootstrap.min.css" rel="stylesheet">
        <link href="/<?php echo __ROOT_BASE__ ?>/Assets/css/font-awesome.min.css" rel="stylesheet">
<!--        <link href="Assets/css/pe-icons.css" rel="stylesheet">-->
<!--        <link href="Assets/css/prettyPhoto.css" rel="stylesheet">-->
<!--        <link href="Assets/css/animate.css" rel="stylesheet">-->
        <link href="/<?php echo __ROOT_BASE__ ?>/Assets/css/style.css" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="/<?php echo __ROOT_BASE__ ?>/Assets/css/dataTables.bootstrap.min.css">
    </head>
    <body>

        <header class="navbar navbar-inverse navbar-fixed-top " role="banner">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <i class="fa fa-bars"></i>
                    </button>
                    <a class="navbar-brand" href="/<?php echo __ROOT_BASE__ ;?>"><h1><span class="pe-7s-gleam bounce-in"></span>  Unir </h1></a>
                </div>
                <div class="collapse navbar-collapse">
                    <ul class="nav navbar-nav navbar-right">
                        <li><a href="/<?php echo __ROOT_BASE__ ;?>">Inicio</a></li>
                        <?php
                            if(@$_COOKIE['users'])
                            {
                                $user = @$_COOKIE['users'];
                                ?>
                                <li><a href="/<?php echo __ROOT_BASE__ ;?>/matricula">Matricular</a></li>
                                <li><a href="/<?php echo __ROOT_BASE__ ?>/notas">Notas</a></li>
                                <li class="dropdown active">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Administrador <i class="icon-angle-down"></i></a>
                                    <ul class="dropdown-menu">
                                        <li><a href="/<?php echo __ROOT_BASE__ ?>/estudiantes">Estudiantes</a></li>
                                        <li><a href="/<?php echo __ROOT_BASE__ ?>/materias">Materias</a></li>
                                        <li><a href="/<?php echo __ROOT_BASE__ ?>/users">Users</a></li>
                                    </ul>
                                </li>
                                <li>
                                    <a href="/<?php echo __ROOT_BASE__ ?>/logout">Cerrar Sesion</a>
                                </li>
                                <?php
                                }
                            ?>

                    </ul>
                </div>
            </div>
        </header>

        <div id="content-wrapper">
            <section class="white" >
                <?php
                if(@$_COOKIE['users'])
                {
                    ?>
                    <div class="container-fluid row" style="margin-top: 10px;">
                        <div class="col col-md-12" style="background-color: #9d9d9d">
                            <p style="color: white; font-style: italic">Bienvenid@ <b><?php echo $user; ?></b></p>
                        </div>
                    </div>
                    <?php
                }
                ?>
                <div class="container">