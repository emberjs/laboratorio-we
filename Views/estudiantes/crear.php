
<?php
    include_once ('Views/header.php');
?>


<div class="container">

    <div class="row">
        <div class="col-xs-12 col-sm-8 col-md-6 col-sm-offset-2 col-md-offset-3">
            <form role="form" id="estudentForm" method="post" action="/<?php echo __ROOT_BASE__ ?>/estudiantes/guardar">
                <h2>Registrar Estudiante</h2>
                <hr class="colorgraph">
                <div class="row">
                    <div class="col-xs-12 col-sm-6 col-md-6">
                        <div class="form-group">
                            <label for="numdocumento">DNI</label>
                            <input type="number" name="numdocumento" id="numdocumento" class="form-control input-lg" placeholder="DNI" tabindex="1" required>
                            <span style="color: red;"  id="errorDni"></span>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-6">
                        <div class="form-group">
                            <label for="nombres">Nombres</label>
                            <input type="text" name="nombres" id="nombres" class="form-control input-lg" placeholder="Nombres" tabindex="2" required>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 col-sm-6 col-md-6">
                        <div class="form-group">
                            <label for="apellidos">Apellidos</label>
                            <input type="text" name="apellidos" id="apellidos" class="form-control input-lg" placeholder="Apellidos" tabindex="3" required>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-6">
                        <div class="form-group">
                            <label for="direccion">Direccion</label>
                            <textarea id="direccion" class="form-control input-lg" name="direccion" rows="4" cols="50" placeholder="Direccion" tabindex="4" required></textarea>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 col-sm-6 col-md-6">
                        <div class="form-group">
                            <label for="telefonoFijo">Telefono Fijo</label>
                            <input type="number" name="telefonoFijo" id="telefonoFijo" class="form-control input-lg" placeholder="Telefono Fijo" tabindex="5" required>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-6">
                        <div class="form-group">
                            <label for="telefonoMovil">Telefono Movil</label>
                            <input type="number" name="telefonoMovil" id="telefonoMovil" class="form-control input-lg" placeholder="Telefono Movil" tabindex="6" required>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-4 col-sm-3 col-md-3">
                            <button id="submit" type="submit" class="btn btn-success" data-color="info" tabindex="7">Guardar</button>
                    </div>
                    <div class="col-xs-4 col-sm-3 col-md-3">
                        <a href="/<?php echo __ROOT_BASE__ ; ?>/estudiantes" class="btn btn-info" data-color="info" tabindex="7">Atras</a>
                    </div>
                </div>
            </form>
        </div>
    </div>


<?php
    include_once ('Views/footer.php');
?>

    <script>

        $('#submit').attr("disabled", true);
        $('#numdocumento').keyup(function(event){
            documento = $('#numdocumento').val();
            url = <?php echo "'/".__ROOT_BASE__."'"; ?> + "/ajax/alumnos";
            if(documento.length >= 8)
            {
                $.ajax({
                    url: url,
                    method: 'post',
                    data: {'id' : documento}
                }).done(function(data) {
                    data = JSON.parse(data);
                    if(data.exist == true)
                    {
                        console.log(data.exist);
                        $('#submit').attr("disabled", true);
                        $("#errorDni").html('El DNI ya esta en uso')
                    }else{
                        console.log(data.exist);
                        $('#submit').attr("disabled", false);
                        $("#errorDni").html('')
                    }
                });
            }else{
                $("#errorDni").html('El DNI debe tener minimo 8 numeros')
                $('#submit').attr("disabled", true);
            }

        });


    </script>