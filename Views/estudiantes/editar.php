
<?php
include_once ('Views/header.php');
?>


<div class="container">

    <div class="row">
        <div class="col-xs-12 col-sm-8 col-md-6 col-sm-offset-2 col-md-offset-3">
            <form role="form" method="post" action="/<?php echo __ROOT_BASE__ ?>/estudiantes/<?php echo $identify ; ?>/guardar">
                <h2>Editar Estudiante</h2>
                <hr class="colorgraph">
                <?php
                    if($data)
                    {
                        ?>
                        <input type="hidden" value="<?php echo $identify ; ?>">
                            <div class="row">
                                <div class="col-xs-12 col-sm-6 col-md-6">
                                    <div class="form-group">
                                        <label for="numdocumento">DNI</label>
                                        <input disabled type="number" name="numdocumento" id="numdocumento" class="form-control input-lg" placeholder="DNI" tabindex="1" value="<?php echo $data[0]['identificasion']; ?>" required>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-6 col-md-6">
                                    <div class="form-group">
                                        <label for="nombres">Nombres</label>
                                        <input type="text" name="nombres" id="nombres" class="form-control input-lg" placeholder="Nombres" tabindex="2" value="<?php echo $data[0]['nombres']; ?>" required>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-sm-6 col-md-6">
                                    <div class="form-group">
                                        <label for="apellidos">Apellidos</label>
                                        <input type="text" name="apellidos" id="apellidos" class="form-control input-lg" placeholder="Apellidos" tabindex="3"  value="<?php echo $data[0]['apellidos']; ?>" required>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-6 col-md-6">
                                    <div class="form-group">
                                        <label for="direccion">Direccion</label>
                                        <textarea id="direccion" class="form-control input-lg" name="direccion" rows="4" cols="50" placeholder="Direccion" tabindex="4"  required><?php echo $data[0]['direccion']; ?></textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-sm-6 col-md-6">
                                    <div class="form-group">
                                        <label for="telefonoFijo">Telefono Fijo</label>
                                        <input type="number" name="telefonoFijo" id="telefonoFijo" class="form-control input-lg" placeholder="Telefono Fijo" tabindex="5" value="<?php echo $data[0]['telefonoFijo']; ?>" required>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-6 col-md-6">
                                    <div class="form-group">
                                        <label for="telefonoMovil">Telefono Movil</label>
                                        <input type="number" name="telefonoMovil" id="telefonoMovil" class="form-control input-lg" placeholder="Telefono Movil" tabindex="6"  value="<?php echo $data[0]['telefonoMovil']; ?>" required>
                                    </div>
                                </div>
                            </div>

                        <?php
                    }
                    else
                    {
                        ?>
                            <div class="alert alert-info" role="alert">
                                El Alumno buscado no existe
                            </div>
                        <?php
                    }
                ?>

                <div class="row">
                    <?php
                        if($data)
                        {
                            ?>
                                <div class="col-xs-4 col-sm-3 col-md-3">
                                    <button type="submit" class="btn btn-success" data-color="info" tabindex="7">Guardar</button>
                                </div>
                            <?php
                        }
                    ?>

                    <div class="col-xs-4 col-sm-3 col-md-3">
                        <a href="/<?php echo __ROOT_BASE__ ; ?>/estudiantes" class="btn btn-info" data-color="info" tabindex="7">Atras</a>
                    </div>
                </div>
            </form>
        </div>
    </div>

    <?php
    include_once ('Views/footer.php');
    ?>

