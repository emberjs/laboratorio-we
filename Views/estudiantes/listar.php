<?php
include_once ('Views/header.php');
?>
    <div class="page-header">
        <h1>Estudiantes</h1>
    </div>
    <div class="col col-md-12">
        <div>
            Agregar <a href="/<?php echo __ROOT_BASE__ ?>/estudiantes/crear" class="btn btn-success"> <i class="glyphicon glyphicon-plus-sign"></i></a>
        </div>
        <br>
        <table id="example" class="table table-striped table-bordered" style="width:100%">
            <thead>
                <tr>
                    <th>Identificasion</th>
                    <th>Nombres</th>
                    <th>Apellidos</th>
                    <th>Direccion</th>
                    <th>Estado</th>
                    <th>Acciones</th>
                </tr>
            </thead>
            <tbody>
                <?php
                    foreach ($data as $item) {
                        $estado = 'inactive';
                        if($item['estado'] == 1 )
                        {
                            $estado = 'active';
                        }
                        ?>
                            <tr>
                                <td><?php echo $item['identificasion']; ?></td>
                                <td><?php echo $item['nombres']; ?></td>
                                <td><?php echo $item['apellidos']; ?></td>
                                <td><?php echo $item['direccion']; ?></td>
                                <td><?php echo $estado; ?></td>
                                <td>
                                    <a class="btn btn-primary" href="/<?php echo __ROOT_BASE__ ?>/estudiantes/<?php echo$item['id']; ?>/editar"><i class="glyphicon glyphicon-pencil"></i></a>
                                    <a class="btn btn-danger" href="/<?php echo __ROOT_BASE__ ?>/estudiantes/<?php echo$item['id']; ?>/eliminar"><i class="glyphicon glyphicon-remove-sign"></i></a>
                                    <a class="btn btn-warning" href="/<?php echo __ROOT_BASE__ ?>/estudiantes/<?php echo$item['id']; ?>/ver"><i class="glyphicon glyphicon-search"></i></a>
                                </td>
                            </tr>
                        <?php
                    }
                ?>

            </tbody>
        </table>
    </div>



<?php
include_once ('Views/footer.php');
?>

<script>
    $('#example').DataTable();
</script>

