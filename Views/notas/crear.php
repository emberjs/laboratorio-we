
<?php
include_once ('Views/header.php');
?>


<div class="container">

    <div class="row">
        <div class="col-xs-12 col-sm-8 col-md-6 col-sm-offset-2 col-md-offset-3">
            <form role="form" id="estudentForm" method="post" action="/<?php echo __ROOT_BASE__ ?>/notas/guardar">
                <h2>Calificar Estudiantes</h2>
                <hr class="colorgraph">
                <span style="color: red;"  id="errorNotas"></span>
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="form-group">
                            <label for="alumno">Estudiante / Materia</label>
                            <select class="form-control input-lg" id="estudiantemateria" name="estudiantemateria">
                                <option value="0">Seleccione uno</option>
                                <?php
                                foreach ($matricula as $value)
                                {
                                    ?>
                                    <option value="<?php  echo $value['id'];?>"> ( <?php echo $value['materia'] ?> ) / <?php echo $value['dni'] ?> - <?php echo $value['nombres'] ?> <?php echo $value['apellidos'] ?> </option>
                                    <?php
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-xs-4 col-sm-4 col-md-4">
                        <div class="form-group">
                            <label for="nota1">Nota 1</label>
                            <input type="number" name="nota1" id="nota1" class="form-control input-lg" placeholder="Nota 1" tabindex="2" required>
                        </div>
                    </div>
                    <div class="col-xs-4 col-sm-4 col-md-4">
                        <div class="form-group">
                            <label for="nota2">Nota 2</label>
                            <input type="number" name="nota2" id="nota2" class="form-control input-lg" placeholder="Nota 2" tabindex="3" required>
                        </div>
                    </div>
                    <div class="col-xs-4 col-sm-4 col-md-4">
                        <div class="form-group">
                            <label for="nota1">Nota 3</label>
                            <input type="number" name="nota3" id="nota3" class="form-control input-lg" placeholder="Nota 3" tabindex="4" required>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-4 col-sm-3 col-md-3">
                        <button id="submit" type="submit" class="btn btn-success" data-color="info" tabindex="7">Guardar</button>
                    </div>
                    <div class="col-xs-4 col-sm-3 col-md-3">
                        <a href="/<?php echo __ROOT_BASE__ ; ?>/notas" class="btn btn-info" data-color="info" tabindex="7">Atras</a>
                    </div>
                </div>
            </form>
        </div>
    </div>


    <?php
    include_once ('Views/footer.php');
    ?>

    <script>

        $('#submit').attr("disabled", true);

        $( "#estudiantemateria" ).change(function(event) {
           var estudiantemateria = event.currentTarget.value;
           validate(estudiantemateria);
        });

        function validate(estudiantemateria) {
            // documento = $('#numdocumento').val();
            var info = {
                'estudiantemateria': estudiantemateria
            };

            url = <?php echo "'/" . __ROOT_BASE__ . "'"; ?> +"/ajax/notas";
            if(estudiantemateria != 0)
            {
                $.ajax({
                    url: url,
                    method: 'post',
                    data: info
                }).done(function (data) {
                    data = JSON.parse(data);
                    if(data.exist == true)
                    {
                        $('#submit').attr("disabled", true);
                        $("#errorNotas").html('El Alumno ya tiene notas para esa materia')
                    }else{
                        $('#submit').attr("disabled", false);
                        $("#errorNotas").html('')
                    }
                });
            }
        }
    </script>
