
<?php
include_once ('Views/header.php');
?>


<div class="container">

    <div class="row">
        <div class="col-xs-12 col-sm-8 col-md-6 col-sm-offset-2 col-md-offset-3">
            <?php
                if($info)
                {
                    ?>
                        <form role="form" id="estudentForm" method="post" action="/<?php echo __ROOT_BASE__ ?>/notas/<?php echo $idInfo ?>/guardar">
                        <h2>Editar Calificasiones</h2>
                        <hr class="colorgraph">
                        <span style="color: red;"  id="errorNotas"></span>
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <div class="form-group">
                                    <div class="alert alert-info">
                                        <strong><?php echo $info[0]['materia'] ?></strong>
                                        <?php echo $info[0]['dni'] ?> - <?php echo $info[0]['nombres'] ?> <?php echo $info[0]['apellidos'] ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-4 col-sm-4 col-md-4">
                                <div class="form-group">
                                    <label for="nota1">Nota 1</label>
                                    <input type="number" name="nota1" id="nota1" class="form-control input-lg" value="<?php echo $info[0]['nota1'] ?>" placeholder="Nota 1" tabindex="2" required>
                                </div>
                            </div>
                            <div class="col-xs-4 col-sm-4 col-md-4">
                                <div class="form-group">
                                    <label for="nota2">Nota 2</label>
                                    <input type="number" name="nota2" id="nota2" class="form-control input-lg" value="<?php echo $info[0]['nota2'] ?>"  placeholder="Nota 2" tabindex="3" required>
                                </div>
                            </div>
                            <div class="col-xs-4 col-sm-4 col-md-4">
                                <div class="form-group">
                                    <label for="nota1">Nota 3</label>
                                    <input type="number" name="nota3" id="nota3" class="form-control input-lg" value="<?php echo $info[0]['nota3'] ?>" placeholder="Nota 3" tabindex="4" required>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-4 col-sm-3 col-md-3">
                                <button id="submit" type="submit" class="btn btn-success" data-color="info" tabindex="7">Guardar</button>
                            </div>
                            <div class="col-xs-4 col-sm-3 col-md-3">
                                <a href="/<?php echo __ROOT_BASE__ ; ?>/notas" class="btn btn-info" data-color="info" tabindex="7">Atras</a>
                            </div>
                        </div>
                    </form>
                    <?php
                }
                else
                {
                    ?>
                    <div class="alert alert-info">
                        <strong>Error!</strong>
                        El estudiante que busca no tiene notas aignadas para la materia seleccioanda.
                        <a href="/<?php echo __ROOT_BASE__ ; ?>/notas" class="btn btn-info" data-color="info" tabindex="7">Atras</a>
                    </div>
                    <?php
                }
            ?>
        </div>
    </div>


    <?php
    include_once ('Views/footer.php');
    ?>

