<?php
include_once ('Views/header.php');
?>
<div class="page-header">
    <h1>Detalles de notas</h1>
</div>

<div class="row" style="margin: 0 auto" >
    <div class="col-xs-12 col-sm-8 col-md-6 col-sm-offset-2 col-md-offset-3" >
        <?php
        if($data)
        {
            ?>
            <div class="panel panel-primary" style="margin: 0 auto">
                <div class="panel-heading">
                    <h3 class="panel-title"><?php echo $data[0]['nombres'] ?>  <?php echo $data[0]['apellidos'] ?></h3>
                </div>
                <div class="panel-body">
                    <div>
                        <b>DNI :</b>  <?php  echo $data[0]['dni'] ?>
                    </div>
                    <div>
                        <b>Nombres y Apellidos :</b>  <?php echo $data[0]['nombres'] ?>  <?php echo $data[0]['apellidos'] ?> ?>
                    </div>
                    <div>
                        <b>Materia :</b>  <?php  echo $data[0]['materia'] ?>
                    </div>
                    <div>
                        <b>Notas :</b>  <?php  echo $data[0]['nota1'] ?> - <?php  echo $data[0]['nota2'] ?> - <?php  echo $data[0]['nota3'] ?>
                    </div>
                    <div>
                        <b>Calificacion :</b> <?php  echo $data[0]['calificacion'] ?>
                    </div>
                </div>
            </div>
            <?php
        }
        else
        {
            ?>
            <div class="alert alert-info" role="alert">
                El Alumno buscado no niene notas para la materia seleccionada
            </div>
            <?php
        }
        ?>
    </div>
    <div class="clearfix"></div>
    <div class="col-xs-12 col-sm-8 col-md-6 col-sm-offset-2 col-md-offset-3">
        <a href="/<?php echo __ROOT_BASE__ ; ?>/notas" class="btn btn-info" data-color="info" tabindex="7">Atras</a>
    </div>
</div>

<?php
include_once ('Views/footer.php');
?>
