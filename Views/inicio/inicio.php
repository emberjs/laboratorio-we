<?php
include_once ('Views/header.php');
?>

<div id="myCarousel" class="carousel slide" data-ride="carousel" style="max-height: 500px;">
    <!-- Indicators -->
    <ol class="carousel-indicators">
        <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
        <li data-target="#myCarousel" data-slide-to="1"></li>
        <li data-target="#myCarousel" data-slide-to="2"></li>
    </ol>

    <!-- Wrapper for slides -->
    <div class="carousel-inner">
        <div class="item active">
            <img src="/<?php echo __ROOT_BASE__ ?>/Assets/images/inicio1.jpg" alt="Los Angeles" style="width:100%; height: 500px">
        </div>

        <div class="item">
            <img src="/<?php echo __ROOT_BASE__ ?>/Assets/images/inicio2.jpg" alt="Chicago" style="width:100%; height: 500px">
        </div>

        <div class="item">
            <img src="/<?php echo __ROOT_BASE__ ?>/Assets/images/inicio3.jpg" alt="New york" style="width:100%; height: 500px">
        </div>
    </div>
</div>
<?php
include_once ('Views/footer.php');
?>
