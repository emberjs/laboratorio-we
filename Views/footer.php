                </div>
            </section>
        </div>

        <footer class="footer">
            <div class="container-fluid" style="margin: 0 auto; background-color: #080808; color: white">
                <div style="text-align: center; padding: 1%">
                    Desarrollada por : <b>Jesson Ember Bejarano Mosquera </b> & <b>Carlos Alberto Hernandez Rincon</b>
                </div>

            </div>
        </footer>

        <script src="/<?php echo __ROOT_BASE__ ?>/Assets/js/jquery.js"></script>
        <script src="/<?php echo __ROOT_BASE__ ?>/Assets/js/html5shiv.js"></script>
        <script src="/<?php echo __ROOT_BASE__ ?>/Assets/js/respond.min.js"></script>
        <script src="/<?php echo __ROOT_BASE__ ?>/Assets/js/plugins.js"></script>
        <script src="/<?php echo __ROOT_BASE__ ?>/Assets/js/bootstrap.min.js"></script>
        <script src="/<?php echo __ROOT_BASE__ ?>/Assets/js/jquery.prettyPhoto.js"></script>
        <script src="/<?php echo __ROOT_BASE__ ?>/Assets/js/init.js"></script>
        <script type="text/javascript" charset="utf8" src="/<?php echo __ROOT_BASE__ ?>/Assets/js/jquery.dataTables.min.js"></script>
        <script src="/<?php echo __ROOT_BASE__ ?>/Assets/js/dataTables.bootstrap.min.js"></script>

<!--        <script type="text/javascript" charset="utf8" src="Assets/js/dataTables.bootstrap4.min"></script>-->

    </body>
</html>

