<?php
//Controlador para manejar todas las interacciones y operaciones vinculadas a las matriculas del sistema

require_once ('Interface/ControllerInterface.php');
require_once ('Models/MatriculaModel.php');
require_once ('Models/EstudiantesModel.php');
require_once ('Models/MateriasModel.php');

class MatriculaController implements ControllerInterface
{

    private $model;
    public function __construct()
    {
        $this->model = new MatriculaModel();
    }

    /*
       *  Listar
       * Metodo del controlador que se encarga de procesar la solicitus de lista de todas las matriculas del sistema
       * @parameter []
       * @Route(/matricula)
    * */
    public function listar()
    {
        $data =  $this->model->getAll();
        include_once ('Views/matricula/listar.php');
    }

    /*
         *  Crear
         * Metodo del controlador que se encarga de procesar la solicitud de abrir la vista para realizar matriculas
         * @parameter []
         * @Route(/matricula/crear)
     * */
    public function crear()
    {
        $estudiantesModel = new EstudiantesModel();
        $materiasService = new MateriasModel();
        $estudiantes = $estudiantesModel->getAll();
        $materias = $materiasService->getMateriasAll();
        include_once ('Views/matricula/crear.php');
    }

    /*
        *  Editar
        * Metodo del controlador que se encarga de procesar la solicitud relacionadas a la edicion de algun matricula del sistema
        * @parameter [$id]
        * @Route(/matricula/{id}/editar)
    * */
    public function editar($id)
    {
        // TODO: Implement editar() method.
    }

    /*
        *  Eliiminar
        * Metodo del controlador que se encarga de procesar la solicitud de eliminar a alguna matricula del sistema
        * @parameter [$id]
        * @Route(/matricula/{$id}/eliminar)
    * */
    public function eliminar($id)
    {
        $data =  $this->model->eliminar($id);
        if($data){
            header("Location: /". __ROOT_BASE__ ."/matricula");
        }
        else
        {
            $this->error404();
        }
    }

    /*
    *  Ver
        * Metodo del controlador que se encarga de procesar la solicitud de ver informacion detallada de alguna matricula
        * @parameter [$id]
        * @Route(/matricula/{$id}/ver)
    * */
    public function ver($id)
    {
        // TODO: Implement ver() method.
    }

    /*
       *  Guardar
       * Metodo del controlador que se encarga de procesar todas las solicitudes POST de edicion o creacion de una nueva matricula
       * @parameter [$id = null]
       * @Route(/matricula/{$id}/guardar)
       * @Route(/matricula/guardar)
   * */
    public function guardar($id = null)
    {
        $data = $this->model->crear($_POST);
        header("Location: /". __ROOT_BASE__ ."/matricula");
    }

    /*
       *  Error404
       * Metodo del controlador que se encarga de mostrar la pagina de error 404 si falla algo del sistema
       * @parameter []
    * */
    public function error404()
    {
        include_once ('Views/error_404.php');
    }

    public function ajax()
    {
        $data = $this->model->find($_POST);
        $return = [
            'exist' => $data
        ];
        echo json_encode($return);
    }
}