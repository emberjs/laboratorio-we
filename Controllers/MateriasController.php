<?php
//Controlador para manejar todas las interacciones y operaciones vinculadas a los materias del sistema
require_once ('Models/MateriasModel.php');
require_once ('Interface/ControllerInterface.php');

class MateriasController implements ControllerInterface
{

    private $MateriasModel;
    public function __construct()
    {
        $this->MateriasModel = new MateriasModel();
    }

    /*
         * Listar
         * Metodo del controlador que se encarga de procesar la solicitus de lista de todas las materias
         * @parameter []
         * @Route(/materias)
     */
    public function listar()
    {
        $data = $this->MateriasModel->getMateriasAll();
        include_once ('Views/materias/listar.php');
    }

    /*
         * Crear
         * Metodo del controlador que se encarga de procesar la solicitud de abrir la vista para registrar Materias
         * @parameter []
         * @Route(/materias/crear)
     * */
    public function crear()
    {
       include_once ('Views/materias/crear.php');
    }

    /*
        * Editar
        * Metodo del controlador que se encarga de procesar la solicitud relacionadas a la edicion de alguna Materia del sistema
        * @parameter [$id]
        * @Route(/materias/{id}/editar)
    * */
    public function editar($id)
    {
        $data =  $this->MateriasModel->findOneById($id);
        $identify = $id;
        include_once ('Views/materias/editar.php');
    }

    /*
        * Eliminar
        * Metodo del controlador que se encarga de procesar la solicitud de eliminar a alguna materia del sistema
        * @parameter [$id]
        * @Route(/materia/{$id}/eliminar)
    * */
    public function eliminar($id)
    {
        $data =  $this->MateriasModel->eliminar($id);
        if($data){
            header("Location: /". __ROOT_BASE__ ."/materias");
        }
        else
        {
            $this->error404();
        }
    }

    /*
        * Ver
        * Metodo del controlador que se encarga de procesar la solicitud de ver informacion detallada de la materia
        * @parameter [$id]
        * @Route(/materias/{$id}/ver)
    * */
    public function ver($id)
    {
        $data =  $this->MateriasModel->findOneById($id);

        include_once ('Views/materias/ver.php');


    }

    /*
       * Guardar
       * Metodo del controlador que se encarga de procesar todas las solicitudes POST de edicion o creacion de una nueva materia
       * @parameter [$id = null]
       * @Route(/materias/{$id}/guardar)
       * @Route(/materias/guardar)
   * */
    public function guardar($id = null)
    {
        if(!$id)
        {
           $data =  $this->MateriasModel->crear($_POST);
        }
        else
        {
            $data =  $this->MateriasModel->actualizar($_POST, $id);
        }
        if($data){
            header("Location: /". __ROOT_BASE__ ."/materias");
        }
        else
        {
            $this->error404();
        }
    }

    /*
        * Error404
        * Metodo del controlador que se encarga de mostrar la pagina de error 404 si falla algo del sistema
        * @parameter []
    * */
    public function error404()
    {
       include_once ('Views/error_404.php');
    }


    public function ajax()
    {
        $data = $this->MateriasModel->finByDescripcion($_POST['id']);
        $return = [
            'exist' => $data
        ];
        echo json_encode($return);
    }

}