<?php
//Controlador para manejar todas las interacciones y operaciones vinculadas a los alumnos del sistema
require_once ('Models/EstudiantesModel.php');
require_once ('Interface/ControllerInterface.php');

class EstudiantesController implements ControllerInterface
{


    private $estudiantesModel;
    public function __construct()
    {
        $this->estudiantesModel = new EstudiantesModel();
    }

    /*
        *  Listar
        * Metodo del controlador que se encarga de procesar la solicitus de lista de todos los estudiantes del sistema
        * @parameter []
        * @Route(/estudiantes)
    * */
    public function listar()
    {
        $data = $this->estudiantesModel->getAll();
        include_once ('Views/estudiantes/listar.php');
    }

    /*
         *  Crear
         * Metodo del controlador que se encarga de procesar la solicitud de abrir la vista para registrar estudiantes
         * @parameter []
         * @Route(/estudiantes/crear)
     * */
    public function crear()
    {
        include_once ('Views/estudiantes/crear.php');
    }

    /*
        *  Editar
        * Metodo del controlador que se encarga de procesar la solicitud relacionadas a la edicion de algun estudiante del sistema
        * @parameter [$id]
        * @Route(/estudiantes/{id}/editar)
    * */
    public function editar($id)
    {
        $data =  $this->estudiantesModel->findOneById($id);
        $identify = $id;
        include_once ('Views/estudiantes/editar.php');
    }

    /*
        *  Eliiminar
        * Metodo del controlador que se encarga de procesar la solicitud de eliminar a algun estudiante del sistema
        * @parameter [$id]
        * @Route(/estudiantes/{$id}/eliminar)
    * */
    public function eliminar($id)
    {
        $data =  $this->estudiantesModel->eliminar($id);
        if($data){
            header("Location: /". __ROOT_BASE__ ."/estudiantes");
        }
        else
        {
            $this->error404();
        }
    }

    /*
    *  Ver
        * Metodo del controlador que se encarga de procesar la solicitud de ver informacion detallada del estudiante
        * @parameter [$id]
        * @Route(/estudiantes/{$id}/ver)
    * */
    public function ver($id)
    {
        $data =  $this->estudiantesModel->findOneById($id);

        include_once ('Views/estudiantes/ver.php');

        # code...
    }

    /*
       *  Guardar
       * Metodo del controlador que se encarga de procesar todas las solicitudes POST de edicion o creacion de un nuevo estudiante
       * @parameter [$id = null]
       * @Route(/estudiantes/{$id}/guardar)
       * @Route(/estudiantes/guardar)
   * */
    public function guardar($id = null)
    {
        if(!$id)
        {
            $data =  $this->estudiantesModel->crear($_POST);
        }
        else
        {
            $data =  $this->estudiantesModel->actualizar($_POST, $id);
        }
        if($data){
            header("Location: /". __ROOT_BASE__ ."/estudiantes");
        }
        else
        {
            $this->error404();
        }
    }

    /*
        *  Error404
        * Metodo del controlador que se encarga de mostrar la pagina de error 404 si falla algo del sistema
        * @parameter []
    * */
    public function error404()
    {
        include_once ('Views/error_404.php');
    }


    public function ajax()
    {
        $data = $this->estudiantesModel->finByDNI($_POST['id']);
        $return = [
            'exist' => $data
        ];
        echo json_encode($return);
    }
}