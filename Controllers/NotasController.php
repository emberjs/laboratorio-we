<?php
//Controlador para manejar todas las interacciones y operaciones vinculadas a las notas del sistema

require_once ('Models/NotasModel.php');
require_once ('Interface/ControllerInterface.php');
require_once ('Models/MatriculaModel.php');

class NotasController implements ControllerInterface
{

    private $model;
    public function __construct()
    {
        $this->model = new NotasModel();
    }

    /*
       *  Listar
       * Metodo del controlador que se encarga de procesar la solicitus de lista de todas las notas del sistema
       * @parameter []
       * @Route(/notas)
    * */
    public function listar()
    {
        $data = $this->model->getAll();
        include_once ('Views/notas/listar.php');
    }

    /*
         *  Crear
         * Metodo del controlador que se encarga de procesar la solicitud de abrir la vista para registrar notas
         * @parameter []
         * @Route(/notas/crear)
     * */
    public function crear()
    {
        $matriculaModel = new MatriculaModel();
        $matricula = $matriculaModel->getAll();
        include_once ('Views/notas/crear.php');
    }

    /*
        *  Editar
        * Metodo del controlador que se encarga de procesar la solicitud relacionadas a la edicion de alguna nota del sistema
        * @parameter [$id]
        * @Route(/notas/{id}/editar)
    * */
    public function editar($id)
    {
        $info = $this->model->findOneById($id);
        $idInfo = $id;
        include_once ('Views/notas/editar.php');
    }

    /*
        *  Eliiminar
        * Metodo del controlador que se encarga de procesar la solicitud de eliminar a alguna nota del sistema
        * @parameter [$id]
        * @Route(/notas/{$id}/eliminar)
    * */
    public function eliminar($id)
    {
        $data = $this->model->eliminar($id);

        if($data){
            header("Location: /". __ROOT_BASE__ ."/notas");
        }
        else
        {
            $this->error404();
        }
    }

    /*
    *  Ver
        * Metodo del controlador que se encarga de procesar la solicitud de ver informacion detallada de alguna nota
        * @parameter [$id]
        * @Route(/notas/{$id}/ver)
    * */
    public function ver($id)
    {
        $data = $this->model->findOneById($id);
        include_once ('Views/notas/ver.php');
    }

    /*
       *  Guardar
       * Metodo del controlador que se encarga de procesar todas las solicitudes POST de edicion o creacion de alguna nota
       * @parameter [$id = null]
       * @Route(/notas/{$id}/guardar)
       * @Route(/notas/guardar)
    * */
    public function guardar($id = null)
    {
        if($id)
        {
            $data = $this->model->actualizar($_POST, $id);
        }
        else
        {
            $data = $this->model->crear($_POST);
        }

        if($data)
        {
            header("Location: /". __ROOT_BASE__ ."/notas");
        }
        else
        {
            $this->error404();
        }
        var_dump($data);
    }

    /*
       *  Error404
       * Metodo del controlador que se encarga de mostrar la pagina de error 404 si falla algo del sistema
       * @parameter []
    * */
    public function error404()
    {
        include_once ('Views/error_404.php');
    }


    public function ajax()
    {
        $data = $this->model->findOneByusermateria($_POST['estudiantemateria']);
        $info = false;
        if($data)
        {
            $info = true;
        }
        $return = [
            'exist' => $data
        ];
        echo json_encode($return);
    }
}