<?php
//Controlador para manejar todas las interacciones y operaciones vinculadas a lopagina de inicio del sistema


class InicioController
{
    /*
         *  Login
         * Metodo del controlador que se encarga demostrar la pagina de inicio
         * @parameter []
    * */
    public function get()
    {
        include_once ('Views/inicio/inicio.php');
    }
}