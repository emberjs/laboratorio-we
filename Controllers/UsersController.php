<?php
//Controlador para manejar todas las interacciones y operaciones vinculadas a los usuarios del sistema

require_once ('Interface/ControllerInterface.php');
require_once ('Models/UsersModel.php');

class UsersController implements ControllerInterface
{

    private $model;
    public function __construct()
    {
        $this->model = new UsersModel();
    }

    /*
         *  Listar
         * Metodo del controlador que se encarga de procesar la solicitus de lista de todos los usuarios del sistema
         * @parameter []
         * @Route(/users)
     * */
    public function listar()
    {
        $data = $this->model->getAll();
        include_once ('Views/users/listar.php');
    }

    /*
         *  Crear
         * Metodo del controlador que se encarga de procesar la solicitud de abrir la vista para registrar usuarios
         * @parameter []
         * @Route(/users/crear)
     * */
    public function crear()
    {
        include_once ('Views/users/crear.php');
    }

    /*
        *  Editar
        * Metodo del controlador que se encarga de procesar la solicitud relacionadas a la edicion de algun usuario
        * @parameter [$id]
        * @Route(/users/{id}/editar)
    * */
    public function editar($id)
    {
        $data =  $this->model->findById($id);
        $identify = $id;
        include_once ('Views/users/editar.php');
    }

    public function eliminar($id)
    {
        include_once ('Views/error_404.php');
    }

    public function ver($id)
    {
        include_once ('Views/error_404.php');
    }

    /*
       *  Guardar
       * Metodo del controlador que se encarga de procesar todas las solicitudes POST de edicion o creacion de algun usuario
       * @parameter [$id = null]
       * @Route(/notas/{$id}/guardar)
       * @Route(/notas/guardar)
    * */
    public function guardar($id = null)
    {
        if($id)
        {
            $data = $this->model->actualizar($_POST, $id);
        }
        else
        {
            $data =  $this->model->crear($_POST);
        }

        if($data)
        {
            header("Location: /". __ROOT_BASE__ ."/users");
        }
        else
        {
            $this->error404();
        }
    }

    /*
       *  Error404
       * Metodo del controlador que se encarga de mostrar la pagina de error 404 si falla algo del sistema
       * @parameter []
    * */
    public function error404()
    {
        include_once ('Views/error_404.php');
    }


    public function ajax()
    {
        $data = $this->model->finByUser($_POST['user']);
        $return = [
            'exist' => $data
        ];
        echo json_encode($return);
    }

    /*
      *  Login
      * Metodo del controlador que se encarga de autenticar a los usuarios en el sistema
      * @parameter []
    * */
    public function login()
    {
        $data = null;
        if($_POST && @$_POST['user'] && @$_POST['password'])
        {
           $data = $this->model->login($_POST);

           if(!$data['error'])
           {
               setcookie("users", $data['user']);
               header("Location: /". __ROOT_BASE__ ."/");
           }
        }

        include_once ('Views/users/login.php');

    }

    /*
         *  Login
         * Metodo del controlador que se encarga de eliminar la autenticacion a los usuarios en el sistema
         * @parameter []
    * */
    public function logOut()
    {
        setcookie("users", null);
        header("Location: /". __ROOT_BASE__ ."/");
    }
}