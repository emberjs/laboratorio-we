<?php

/*
    * Modelo que se encargada de realizar todas las operaciones vinculadas a la asignacion de notas
*/
require_once ('DataBase/Operaciones.php');
require_once  ('Services/NotasActions.php');

class NotasModel extends NotasActions
{
    private $connect;

    public function __construct()
    {
        $this->connect = new Operaciones();
    }

    /*
        *  getAll
        * Accion del modelo que se encarga de realizar una consulta contra la base de datos y devolver el listado total de notas
    * */
    public function getAll()
    {
        $sql = "
            SELECT
                alumnos.numdocumento AS dni, 
                alumnos.nombres AS nombres, 
                alumnos.apellidos AS apellidos, 
                materias.descripcion AS materia, 
                notas.nota1 AS nota1, 
                notas.nota2 AS nota2, 
                notas.nota3 AS nota3, 
                notas.id AS id
            FROM
                alumnos
                INNER JOIN
                materiasalumnos
                ON 
                    alumnos.id = materiasalumnos.idalumno
                INNER JOIN
                materias
                ON 
                    materiasalumnos.idmateria = materias.id
                INNER JOIN
                notas
                ON 
                    materiasalumnos.id = notas.idmateriaalumno
		";

        $data = $this->connect->consult($sql);
        $info = [];
        while ($item = mysqli_fetch_array($data))
        {
            $promedio = $this->getPromedio($item['nota1'],$item['nota2'],$item['nota3'] );
            $tempInfo = [];
            $tempInfo['id'] = $item['id'];
            $tempInfo['dni'] = $item['dni'];
            $tempInfo['nombres'] = $item['nombres'];
            $tempInfo['apellidos'] = $item['apellidos'];
            $tempInfo['materia'] = $item['materia'];
            $tempInfo['calificacion'] = $promedio;
            $info[] = $tempInfo;
        }
        return $info;
    }

    /*
        *  findOneById
        * Accion de la clase que se encarga de buscar notas segun el id
    * */
    public function findOneById($id)
    {
        $sql = "
            SELECT
                alumnos.numdocumento AS dni, 
                alumnos.nombres AS nombres, 
                alumnos.apellidos AS apellidos, 
                materias.descripcion AS materia, 
                notas.nota1 AS nota1, 
                notas.nota2 AS nota2, 
                notas.nota3 AS nota3, 
                notas.id AS id
            FROM
                alumnos
                INNER JOIN
                materiasalumnos
                ON 
                    alumnos.id = materiasalumnos.idalumno
                INNER JOIN
                materias
                ON 
                    materiasalumnos.idmateria = materias.id
                INNER JOIN
                notas
                ON 
                    materiasalumnos.id = notas.idmateriaalumno
            WHERE
                notas.id = ". $id ."
        ";
        $data = $this->connect->consult($sql);
        $info = [];
        while ($item = mysqli_fetch_array($data))
        {
            $promedio = $this->getPromedio($item['nota1'],$item['nota2'],$item['nota3'] );
            $tempInfo = [];
            $tempInfo['id'] = $item['id'];
            $tempInfo['dni'] = $item['dni'];
            $tempInfo['nombres'] = $item['nombres'];
            $tempInfo['apellidos'] = $item['apellidos'];
            $tempInfo['materia'] = $item['materia'];
            $tempInfo['calificacion'] = $promedio;
            $tempInfo['nota1'] = $item['nota1'];
            $tempInfo['nota2'] = $item['nota2'];
            $tempInfo['nota3'] = $item['nota3'];
            $info[] = $tempInfo;
        }
        return $info;
    }

    /*
         *  eliminar
         * Accion del modelo que se encarga de eliminar notas
     * */
    public function eliminar($id)
    {
        $sql = "DELETE FROM notas WHERE id = ". $id;
        $data = $this->connect->consult($sql);
        return $data;
    }

    /*
        *  findOneByusermateria
        * Accion de la clase que se encarga de buscar notas segun la materia seleccionada
    * */
    public function findOneByusermateria($id)
    {
        $sql = "SELECT * FROM notas WHERE idmateriaalumno = "."'" .$id . "'";
        $data = $this->connect->consult($sql);
        $count = 0;
        if($data)
        {
            while ($item = mysqli_fetch_array($data))
            {
                $count += 1;
            }
        }
        return $count > 0;
    }

    /*
         *  Crear
         * Accion del modelo que se encarga de realizar la creacion de notas en el sistema
     * */
    public function crear($data)
    {
        $user =  @$_COOKIE['users'];
        $fecha = new DateTime('now');
        $sql = "INSERT INTO notas (
                idmateriaalumno,
                nota1,
                nota2,
                nota3,
                 creadoPor, 
                 fechaCreado
                )";
        $sql .= "VALUES (
                ". "'". trim($data['estudiantemateria']). "'" .", 
                ". "'". trim($data['nota1']). "'" ." ,              
                ". "'". trim($data['nota2']). "'" ." ,              
                ". "'". trim($data['nota3']). "'" ." ,              
                ". "'". $user. "'" ." , 
                ". "'". $fecha->format('Y-m-d'). "'" ." )";

        $data = $this->connect->consult($sql);
        return $data;
    }

    /*
        *  actualizar
        * Accion del modelo que se encarga de realizar la actualizacion de notas en el sistema
    * */
    public function actualizar($data, $id)
    {
        $fecha = new DateTime('now');
        $user =  @$_COOKIE['users'];
        $sql = "UPDATE notas SET 
                    nota1 = ". "'". $data['nota1']. "'" ." , 
                    nota2 = ". "'". $data['nota2']."'" .", 
                    nota3 = ". "'". $data['nota3']."'" .", 
                    modificadoPor =". "'". $user. "'" ." , 
                    fechaModificado = ". "'" . $fecha->format('Y-m-d'). "'" . "
                WHERE 
                id = " . "'" . $id . "'" ;

        $data = $this->connect->consult($sql);
        return $data;
    }
}