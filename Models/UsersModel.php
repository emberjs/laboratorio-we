<?php
/*
    * Modelo que se encargada de realizar todas las operaciones vinculadas a la gestion de usuarios
*/
require_once ('DataBase/Operaciones.php');
require_once ('Services/PasswordOptions.php');

class UsersModel extends PasswordOptions
{
    private $connect;

    public function __construct()
    {
        $this->connect = new Operaciones();
    }

    /*
         *  getAll
         * Accion del modelo que se encarga de realizar una consulta contra la base de datos y devolver el listado total de etsudiantes
     * */
    public function getAll()
    {
        $sql = "SELECT * FROM usuario";
        $data = $this->connect->consult($sql);
        $info = [];
        while ($item = mysqli_fetch_array($data))
        {
            $tempInfo = [];
            $tempInfo['id'] = $item['id'];
            $tempInfo['usuario'] = $item['usuario'];
            $info[] = $tempInfo;
        }
        return $info;
    }

    /*
         *  FindByUser
         * Accion del modelo que se encarga de realizar la busqueda de estudiantes por DNI
     * */
    public function finByUser ($user)
    {
        $sql = "SELECT * FROM usuario WHERE usuario = "."'" .$user . "'";
        $data = $this->connect->consult($sql);
        $count = 0;
        if($data)
        {
            while ($item = mysqli_fetch_array($data))
            {
                $count += 1;
            }
        }
        return $count > 0;
    }

    /*
          *  FindByUser
          * Accion de la clase que se encarga de ejecutar la consulta para buscar usuarios por su user
    * */
    public function findById($id)
    {
        $sql = "SELECT * FROM usuario WHERE id = "."'" .$id . "'";
        $data = $this->connect->consult($sql);
        $info = [];
        while ($item = mysqli_fetch_array($data))
        {
            $tempInfo = [];
            $tempInfo['id'] = $item['id'];
            $tempInfo['usuario'] = $item['usuario'];
            $info[] = $tempInfo;
        }
        return $info;
    }

    /*
         *  login
         * Accion de la clase que se encarga de ejecutar la consultaestablecer la autenticacion del usuario
   * */
    public function login($data)
    {
        $user = $this->finByUser($data['user']);
        $msg = "";
        $error = true;
        $userEmail = $data['user'];
        if($user)
        {
            $info = $this->getUser($data);
            if($info)
            {
                $error = false;
                $userEmail = $info[0]['usuario'];
            }
            else
            {
                $msg = "Contraseña incorrecta";
            }
        }
        else
        {
            $msg = "Usuario / Email incorrecto";
        }

        return [
            'error' => $error,
            'msg' => $msg,
            'user' => $userEmail
        ];
    }

    /*
         *  Crear
         * Accion del modelo que se encarga de realizar la creacion de los usuarios
     * */
    public function crear( array $data)
    {
        $user =  @$_COOKIE['users'];
        $fecha = new DateTime('now');
        $contrasena = $this->encrypt(trim($data['contrasena']));
        $sql = "INSERT INTO usuario (
                usuario,
                clave,
                 creadoPor, 
                 fechaCreado
                )";
        $sql .= "VALUES (
                ". "'". trim($data['usuario']). "'" .", 
                ". "'". $contrasena. "'" ." ,              
                ". "'". $user. "'" ." , 
                ". "'". $fecha->format('Y-m-d'). "'" ." )";

        $data = $this->connect->consult($sql);
        return $data;
    }

    /*
         *  getUser
         * Accion del modelo que se encarga de validar si el usuario ingreso credenciales validas
     * */
    public function getUser($user)
    {
        $password = $this->encrypt($user['password']);
        $sql = "SELECT * FROM usuario WHERE usuario = ". "'". $user['user'] . "' AND clave =". "'". $password . "'";
        $data = $this->connect->consult($sql);
        $info = [];
        while ($item = mysqli_fetch_array($data))
        {
            $tempInfo = [];
            $tempInfo['id'] = $item['id'];
            $tempInfo['usuario'] = $item['usuario'];
            $info[] = $tempInfo;
        }
        return $info;
    }

    /*
         *  Actualizar
         * Accion del modelo que se encarga de realizar el cambio de contraseña de los usuarios
     * */
    public function actualizar($data, $id)
    {
        $fecha = new DateTime('now');
        $user =  @$_COOKIE['users'];
        $password = $this->encrypt($data['contrasena']);
        $sql = "UPDATE usuario SET 
                    clave = ". "'". $password. "'" ." , 
                    modificadoPor =". "'". $user. "'" ." , 
                    fechaModificado = ". "'" . $fecha->format('Y-m-d'). "'" . "
                WHERE 
                id = " . "'" . $id . "'" ;

        $data = $this->connect->consult($sql);
        return $data;
    }

}