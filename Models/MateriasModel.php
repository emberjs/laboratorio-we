<?php
/*
    * Modelo que se encargada de realizar todas las operaciones vinculadas a las Materias
*/
require_once ('DataBase/Operaciones.php');

class MateriasModel extends Operaciones
{

    /*
         *  getMateriasAll
         * Accion del modelo que se encarga de realizar una consulta contra la base de datos y devolver el listado total de Materias
     * */
    public function getMateriasAll()
    {
        $sql = "SELECT * FROM materias";
        $data = $this->consult($sql);
        $info = [];
        while ($item = mysqli_fetch_array($data))
        {
            $tempInfo = [];
            $tempInfo['id'] = $item['id'];
            $tempInfo['descripcion'] = $item['descripcion'];
            $tempInfo['estado'] = $item['estado'];
            $tempInfo['horas'] = $item['horas'];
            $info[] = $tempInfo;
        }
        return $info;
    }

    /*
         *  FindOneById
         * Accion de la clase que se encarga de buscar materias segun su Id
     * */
    public function findOneById($id)
    {
        $sql = "SELECT * FROM materias WHERE id = ". $id;
        $data = $this->consult($sql);
        $info = [];
        while ($item = mysqli_fetch_array($data))
        {
            $tempInfo = [];
            $tempInfo['id'] = $item['id'];
            $tempInfo['descripcion'] = $item['descripcion'];
            $tempInfo['estado'] = $item['estado'];
            $tempInfo['horas'] = $item['horas'];
            $info[] = $tempInfo;
        }
        return $info;
    }

    /*
         *  Crear
         * Accion del modelo que se encarga de realizar la creacion de Materias
     * */
    public function crear( array $data)
    {
        $fecha = new DateTime('now');
        $user = @$_COOKIE['users'];
        $estado = 1;
        $sql = "INSERT INTO materias (
                 descripcion,
                 horas,
                 estado,
                 creado,
                 fechaCreado
                )";
        $sql .= "VALUES (
                ". "'". $data['descripcion']. "'" .", 
                ". "'". $data['horas']. "'" ." , 
                ". "'". $estado. "'" .",  
                ". "'". $user. "'" .",  
                ". "'". $fecha->format('Y-m-d'). "'" ."  
                 )";
        $data = $this->consult($sql);
        return $data;
    }

    /*
         *  actualizar
         * Accion del modelo que se encarga de realizar la Actualizacion de las Materias
     * */
    public function actualizar($data, $id)
    {
        $fecha = new DateTime('now');
        $user = @$_COOKIE['users'];
        $sql = "UPDATE materias SET 
                    horas = ". "'". $data['horas']."'" .", 
                    modificadoPor =". "'". $user. "'" ." , 
                    fechaModificado = ". "'" . $fecha->format('Y-m-d'). "'" . "
                WHERE 
                id = " . "'" . $id . "'" ;

        $data = $this->consult($sql);
        return $data;
    }

    /*
         *  eliminar
         * Accion del modelo que se encarga de eliminar Materias
     * */
    public function eliminar($id)
    {
        $sql = "DELETE FROM materias WHERE id = ". $id;
        $data = $this->consult($sql);
        return $data;
    }

    /*
         *  finByDescripcion
         * Accion del modelo que se encarga de realizar la busqueda de matrias por descripcion
     * */
    public function finByDescripcion ($descripcion)
    {
        $sql = "SELECT * FROM materias WHERE descripcion = ". $descripcion;
        $data = $this->consult($sql);
        $count = 0;
        while ($item = mysqli_fetch_array($data))
        {
            $count += 1;
        }

        return $count > 0;
    }
}