<?php
/*
    * Modelo que se encargada de realizar todas las operaciones vinculadas a los estudiantes
*/
require_once ('DataBase/Operaciones.php');

class EstudiantesModel extends Operaciones
{

    /*
         *  getAll
         * Accion del modelo que se encarga de realizar una consulta contra la base de datos y devolver el listado total de etsudiantes
     * */
    public function getAll()
    {
        $sql = "SELECT * FROM alumnos";
        $data = $this->consult($sql);
        $info = [];
        while ($item = mysqli_fetch_array($data))
        {
            $tempInfo = [];
            $tempInfo['id'] = $item['id'];
            $tempInfo['identificasion'] = $item['numdocumento'];
            $tempInfo['estado'] = $item['estado'];
            $tempInfo['nombres'] = $item['nombres'];
            $tempInfo['apellidos'] = $item['apellidos'];
            $tempInfo['direccion'] = $item['direccion'];
            $info[] = $tempInfo;
        }
        return $info;
    }

    /*
         *  Crear
         * Accion del modelo que se encarga de realizar la creacion de los estudiantes
     * */
    public function crear( array $data)
    {
        $fecha = new DateTime('now');
        $user =  @$_COOKIE['users'];
        $estado = 1;
        $sql = "INSERT INTO alumnos (
                numdocumento,
                 nombres, 
                 apellidos, 
                 direccion, 
                 telefonoFijo, 
                 telefonoMovil, 
                 estado, 
                 creado, 
                 fechaCreado 
                )";
        $sql .= "VALUES (
                ". $data['numdocumento'] .", 
                ". "'". $data['nombres']. "'" ." , 
                ". "'". $data['apellidos']."'" .", 
                ". "'". $data['direccion']."'" .", 
                ". "'". $data['telefonoFijo']."'" .", 
                ". "'". $data['telefonoMovil']."'" .", 
                ". "'". $estado. "'" .",  
                ". "'". $user. "'" .",  
                ". "'". $fecha->format('Y-m-d'). "'" ." )";

        $data = $this->consult($sql);
        return $data;
    }

    /*
         *  actualizar
         * Accion del modelo que se encarga de realizar la Actualizacion de los estudiantes
     * */
    public function actualizar($data, $id)
    {
        $fecha = new DateTime('now');
        $user =  @$_COOKIE['users'];
        $sql = "UPDATE alumnos SET 
                    nombres = ". "'". $data['nombres']. "'" ." , 
                    apellidos = ". "'". $data['apellidos']."'" .", 
                    direccion = ". "'". $data['direccion']."'" .", 
                    telefonoFijo = ". "'". $data['telefonoFijo']."'" .", 
                    telefonoMovil = ". "'". $data['telefonoMovil']."'" .", 
                    modificado =". "'". $user. "'" ." , 
                    fechaModificado = ". "'" . $fecha->format('Y-m-d'). "'" . "
                WHERE 
                id = " . "'" . $id . "'" ;

        $data = $this->consult($sql);
        return $data;
    }

    /*
         *  eliminar
         * Accion del modelo que se encarga de eliminar estudiantes
     * */
    public function eliminar($id)
    {
        $sql = "DELETE FROM alumnos WHERE id = ". $id;
        $data = $this->consult($sql);
        return $data;
    }

    /*
         *  finByDNI
         * Accion del modelo que se encarga de realizar la busqueda de estudiantes por DNI
     * */
    public function finByDNI ($dni)
    {
        $sql = "SELECT * FROM alumnos WHERE numdocumento = ". $dni;
        $data = $this->consult($sql);
        $count = 0;
        if($data)
        {
            while ($item = mysqli_fetch_array($data))
            {
                $count += 1;
            }
        }

        return $count > 0;
    }

    /*
        *  FindOneById
        * Accion de la clase que se encarga de buscar estudiantes segun su Id
    * */
    public function findOneById($id)
    {
        $sql = "SELECT * FROM alumnos WHERE id = ". $id;
        $data = $this->consult($sql);
        $info = [];
        while ($item = mysqli_fetch_array($data))
        {
            $tempInfo = [];
            $tempInfo['id'] = $item['id'];
            $tempInfo['identificasion'] = $item['numdocumento'];
            $tempInfo['estado'] = $item['estado'];
            $tempInfo['nombres'] = $item['nombres'];
            $tempInfo['apellidos'] = $item['apellidos'];
            $tempInfo['direccion'] = $item['direccion'];
            $tempInfo['telefonoFijo'] = $item['telefonoFijo'];
            $tempInfo['telefonoMovil'] = $item['telefonoMovil'];
            $info[] = $tempInfo;
        }
        return $info;
    }
}