<?php
/*
    * Modelo que se encargada de realizar todas las operaciones vinculadas a la matricula de estudiantes
*/
require_once ('DataBase/Operaciones.php');

class MatriculaModel extends Operaciones
{
    /*
        *  getAll
        * Accion del modelo que se encarga de realizar una consulta contra la base de datos y devolver el listado total de matriculas
    * */
    public function getAll()
    {
        $sql = "
            SELECT
                alumnos.numdocumento AS dni, 
                alumnos.nombres AS nombres, 
                alumnos.apellidos AS apellidos, 
                materias.descripcion AS materia,
                materiasalumnos.id AS id
            FROM
                alumnos
                INNER JOIN
                materiasalumnos
                ON 
                    alumnos.id = materiasalumnos.idalumno
                INNER JOIN
                materias
                ON 
                    materiasalumnos.idmateria = materias.id
		";
        $data =$this->consult($sql);
        $info = [];
        while ($item = mysqli_fetch_array($data))
        {
            $tempInfo = [];
            $tempInfo['id'] = $item['id'];
            $tempInfo['dni'] = $item['dni'];
            $tempInfo['nombres'] = $item['nombres'];
            $tempInfo['apellidos'] = $item['apellidos'];
            $tempInfo['materia'] = $item['materia'];
            $info[] = $tempInfo;
        }
        return $info;
    }

    /*
        *  Find
        * Accion de la clase que se encarga de buscar matriculas segun el estudiante y la materia
    * */
    public function find($data)
    {
        $sql = "SELECT * FROM materiasalumnos WHERE idalumno = "."'" .$data['estudiante'] . "' AND idmateria =". "'" .$data['materia'] . "'";
        $data = $this->consult($sql);
        $count = 0;
        if($data)
        {
            while ($item = mysqli_fetch_array($data))
            {
                $count += 1;
            }
        }
        return $count > 0;
    }

    /*
         *  Crear
         * Accion del modelo que se encarga de realizar la creacion matriculas en el sistema
     * */
    public function crear($data)
    {
        $user =  @$_COOKIE['users'];
        $fecha = new DateTime('now');
        $sql = "INSERT INTO materiasalumnos (
                idalumno,
                idmateria,
                estado,
                 creadoPor, 
                 fechaCreado
                )";
        $sql .= "VALUES (
                ". "'". trim($data['alumno']). "'" .", 
                ". "'". trim($data['materia']). "'" ." ,              
                ". "'". 1 . "'" ." ,              
                ". "'". $user. "'" ." , 
                ". "'". $fecha->format('Y-m-d'). "'" ." )";

        $data =$this->consult($sql);
        return $data;
    }

    /*
         *  eliminar
         * Accion del modelo que se encarga de eliminar matriculas
     * */
    public function eliminar($id)
    {
        $sql = "DELETE FROM materiasalumnos WHERE id = ". $id;
        $data = $this->consult($sql);
        return $data;
    }

}