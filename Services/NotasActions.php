<?php
/*
    * Servicio que se encargada de realizar operaciones vinvuladas con los estudiantes
*/

class NotasActions
{
    /*
         *  getPromedio
         * Accion del servicio encargada de realizar el calcula del promedio de las 3 notas del estudiantetotal de etsudiantes
     * */
    public function getPromedio($nota1, $nota2, $nota3)
    {
        return round((($nota1 + $nota2 + $nota3) / 3),2);
    }
}