<?php
/*
    * Servicio que se encargada de realizar operaciones vinvuladas con la contraseña
*/
class PasswordOptions
{
    /*
        *  encrypt
        * Accion del servicio encargada de encryptar la contraseña
    * */
    public function encrypt($password)
    {
        return base64_encode($password);
    }

    /*
        *  encrypt
        * Accion del servicio encargada de desencryptar la contraseña
    * */
    public function decript($password)
    {
        return base64_decode($password);
    }
}