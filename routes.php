<?php
    //Archivo donde se definen todas las rutas del sistema
    //Primero creamos una instancia a todos los controladores a utilizar en el sistema
    require_once ('Controllers/EstudiantesController.php');
    require_once ('Controllers/MateriasController.php');
    require_once ('Controllers/InicioController.php');
    require_once ('Controllers/UsersController.php');
    require_once ('Controllers/MatriculaController.php');
    require_once ('Controllers/NotasController.php');
    require_once ('Interface/ControllerInterface.php');

    //Separamos la url que nos viene en partes y la convertimos en un array
    $url = explode('/', $_SERVER['REQUEST_URI'] ) ;

    //Buscamos las palabras claves de cada una de nuestras rutas averiguamndo si alguna viene para saber que ruta abrir
    $student = array_search('estudiantes', $url);
    $subject = array_search('materias', $url);
    $users = array_search('users', $url);
    $matricula = array_search('matricula', $url);
    $notas = array_search('notas', $url);

    //Antes de empezar con las rutas generales, creamos condicionales para rutas especificas que seran utilizadas para ajax y para cerrar sesion
    $ajaxAlumnos = "/".__ROOT_BASE__."/ajax/alumnos";
    if($_SERVER['REQUEST_URI'] === $ajaxAlumnos && $_SERVER['REQUEST_METHOD'] == 'POST' && @$_COOKIE['users'])
    {
        $controller = new EstudiantesController();
        $controller->ajax();
        return;
    }
    $tajaxMaterias = "/".__ROOT_BASE__."/ajax/materias";
    if($_SERVER['REQUEST_URI'] === $tajaxMaterias && $_SERVER['REQUEST_METHOD'] == 'POST' && @$_COOKIE['users'])
    {
        $controller = new MateriasController();
        $controller->ajax();
        return;
    }
    $ajaxUsers = "/".__ROOT_BASE__."/ajax/users";
    if($_SERVER['REQUEST_URI'] === $ajaxUsers && $_SERVER['REQUEST_METHOD'] == 'POST' && @$_COOKIE['users'])
    {
        $controller = new UsersController();
        $controller->ajax();
        return;
    }

    $ajaxMatricula = "/".__ROOT_BASE__."/ajax/matricula";
    if($_SERVER['REQUEST_URI'] === $ajaxMatricula && $_SERVER['REQUEST_METHOD'] == 'POST' && @$_COOKIE['users'])
    {
        $controller = new MatriculaController();
        $controller->ajax();
        return;
    }

    $ajaxNotas = "/".__ROOT_BASE__."/ajax/notas";
    if($_SERVER['REQUEST_URI'] === $ajaxNotas && $_SERVER['REQUEST_METHOD'] == 'POST' && @$_COOKIE['users'])
    {
        $controller = new NotasController();
        $controller->ajax();
        return;
    }

    $logOut = "/".__ROOT_BASE__."/logout";
    if($_SERVER['REQUEST_URI'] === $logOut && @$_COOKIE['users'] )
    {
        $controller = new UsersController();
        $controller->logOut();
        return;
    }

    //Preguntamos cual de las rutas estoy accediendo y llamo la funcion que abrira la ruta seleccioanda
    if($student && @$_COOKIE['users'])
    {
        $controller = new EstudiantesController();
        abrirController($controller, $student, $url );
    }
    elseif($subject && @$_COOKIE['users'])
    {
        $controller = new MateriasController();
        abrirController($controller, $subject, $url );
    }
    elseif ($users && @$_COOKIE['users'])
    {
        $controller = new UsersController();
        abrirController($controller, $users, $url);
    }
    elseif ($matricula && @$_COOKIE['users'])
    {
       $controller = new MatriculaController();
       abrirController($controller, $matricula, $url);
    }
    elseif ($notas  && @$_COOKIE['users'])
    {
        $controller = new NotasController();
        abrirController($controller, $notas, $url );
    }
    elseif(@$_COOKIE['users'])
    {
        $controller = new InicioController();
        $controller->get();
    }
    else
    {
        $controller = new UsersController();
        $controller->login();
    }

    function abrirController(ControllerInterface $controller, $student, $url)
    {
        $options = [];
        for ($i = $student + 1; $i < count($url); $i++)
        {
            $options[] = $url[$i];
        }

        if($options && count($options) === 2)
        {

            $id = intval($options[0]);
            $actions = $options[1];
            if(is_int($id) && is_string($actions))
            {
                switch ($actions)
                {
                    case 'editar':
                        echo $controller->editar($id);
                        break;
                    case 'eliminar':
                        echo $controller->eliminar($id);
                        break;
                    case 'ver':
                        echo $controller->ver($id);
                        break;
                    case 'guardar':
                        echo $controller->guardar($id);
                        break;
                    default:
                        echo $controller->error404();
                        break;
                }
            }
            else
            {
                echo $controller->error404();
            }
        }
        elseif($options && count($options) === 1)
        {
            $actions = $options[0];

            switch ($actions)
            {
                case 'guardar':
                    echo $controller->guardar();
                    break;
                case 'crear':
                    echo $controller->crear();
                    break;
                default:
                    echo $controller->error404();
                    break;
            }
        }
        else
        {
            $controller->listar();
        }
    }
