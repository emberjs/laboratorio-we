<?php

/*
*  Interface
* Interfas se e encarga de establecer las  operaciones basicas que deben poseer los controladores principales del sistema
* */
interface ControllerInterface
{
    /*
         *  Listar
         * Metodo del controlador que se encarga de realizar todos los procesops relacionados con la lista general de X proceso que se encargue el controlador
         * @parameter []
     * */
    public function listar();

    /*
         *  Listar
         * Metodo del controlador que se encarga de procesar la solicitud de abrir la vista para realizar el registro
         * @parameter []
     * */
    public function crear();

    /*
        *  Editar
        * Metodo del controlador que se encarga de procesar la solicitud relacionadas a la edicion de algun dato del sistema
        * @parameter [$id]
    * */
    public function editar($id);

    /*
        *  Eliiminar
        * Metodo del controlador que se encarga de procesar la solicitud de eliminar a algun dato del sistema
        * @parameter [$id]
    * */
    public function eliminar($id);

    /*
       *  Ver
       * Metodo del controlador que se encarga de procesar la solicitud de ver informacion detallada de algun dato del sistema
       * @parameter [$id]
   * */
    public function ver($id);

    /*
      *  Guardar
      * Metodo del controlador que se encarga de procesar todas las solicitudes POST de edicion o creacion de un nuevo registro
      * @parameter [$id = null]
    * */
    public function guardar($id = null);

    /*
       *  Error404
       * Metodo del controlador que se encarga de mostrar la pagina de error 404 si falla algo del sistema
       * @parameter []
    * */
    public function error404();
}