<?php
//Archivo de configuracion de todas las constantes globales del sistema

$db = array(
    "host"      =>"localhost",
    "user"      =>"root",
    "pass"      =>"",
    "database"  =>"academy",
    "charset"   =>'utf8'
);
//Definimos la constante que contiene la url principal de nuestro sistema
define('__ROOT_BASE__' , 'laboratorio-we');
//Definimos la constante de coneccion a la base de datos
define('__BD_CONNECT__', $db);

 