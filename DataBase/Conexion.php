<?php


class Conexion
{
   
   private $user;
   private $password;
   private$server;
   private $dataBase;

    /*
    *  Constructor
    * Clase que se encarga de relaizar la conexion a la Base de datos
    * */
   public function __construct()
   {
       $db = __BD_CONNECT__;
       $this->user = $db['user'];
       $this->password =  $db['pass'];
       $this->server =  $db['host'];
       $this->dataBase =  $db['database'];
   }

    /*
    *  Connect
    * Metodo de la clase que se encarga de de establecer la conexion de la BD
    * */
    public function connect ()
   {
       $connect = mysqli_connect($this->server, $this->user, $this->password )  or die ("No se ha podido conectar al servidor de Base de datos");
       $db = mysqli_select_db( $connect, $this->dataBase ) or die ( "Upps! Pues va a ser que no se ha podido conectar a la base de datos" );
       return $connect;
   }

}