<?php
require_once ('DataBase/Conexion.php');

class Operaciones
{
    private $connect;

    /*
    *  Constructor
    * Clase que se encarga de realizar las operaciones contra la base de datos
    * */
    public function __construct()
    {
        $this->connect = new Conexion();
    }

    /*
    *  Consult
    *  Metodo de la clase se se encarga de procesar todas las solicitudes SQL del sistema
     * @parameter [$sql]
    * */
    public function consult ($sql)
    {
        $connect = $this->connect->connect();
        $result = mysqli_query( $connect, $sql ) or (404);

        $return = 404;
        if( $result !== 404 )
        {
            $return = $result;
        }
        mysqli_close( $connect );

        return $return;
    }


}